# My Blog Posts

This is the place for storing all sorts of technical articles, tips and 
tricks for programming, especially regarding Python and the use of Git. 
I also include [my resume in PDF format](Yiyang_Shu_Resume_2019-09-15.pdf) 
in this repository for your reference.  

