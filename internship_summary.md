# Internship Experience at _Musicfox_

I interned at [_Musicfox_](https://www.musicfox.io/) during the 
summer of 2019, from June 17th all the way to September 6th, 2019. I 
was working with a brilliant fellow classmate 
[Sherry](https://www.linkedin.com/in/shihui-sherry-yu-50a311107/), 
and we were supervised by [Jason](https://gitlab.com/thinkjrs) and 
[Phil](https://www.linkedin.com/in/philipmuellerschoen/), both of whom 
are super awesome and taught us a lot of cool stuff.  

During the internship, I focus mainly on data engineering and also 
on feature development. More specifically, I'm in charge of supplying 
data to our data scientist and at times of presenting some of the 
findings by developing interactive data visualization applications. 
I accomplish these tasks purely in Python, mainly in Windows Subsystem 
for Linux on Visual Studio Code, occasionaly in Jupyter Lab when I'm 
exploring potential solutions.

This has been an exceptional experience for me and I've learned a ton 
in the process. As such, it makes sense to document and share what 
I've gained in the form of a summary.  

## Summary of What I've Learned  

1. The Git + Bash workflow with GitLab and VSCode 
2. Test-driven development by making use of `pytest`  
3. Interactive web application development with `plotly/dash`  
4. Develop, test, maintain and use our data acquisition library `pycm`  
5. Technical article composition related to Python and Bash  

In the sections to follow, I will describe and share my experience in 
more detail (to the extent allowed in the NDA that is) for each of the 
bullet-points listed above.

## Git + Bash Workflow with GitLab and VSCode 

Prior to my internship here at _Musicfox_, I didn't know much, if 
anything, about either [Git](https://en.wikipedia.org/wiki/Git) or 
[Bash](https://en.wikipedia.org/wiki/Bash_(Unix_shell)). I've only 
been able to manually click and download files from others' repos on 
the GitLab or GitHub. Not that I hadn't tried to make use of the Git by 
command lines, but my previous lack-of-experience with Command-line 
Interface languages in general prevented me from successfully doing 
that. Needless to say, I was also not too competent in Bash, which is a 
command-line language, at all. 

Once we (Sherry and I) jumped onboard, Jason gave us the tour and 
introduced us to the Git and Bash workflow, which I shall briefly do 
the same in this section here.

So to start, the Git is essential for version-control and 
collaborating with others on projects, especially larger and more 
sophisticated ones. The Bash, on the other hand, is a Unix shell and 
command-line language that's useful for navigating through the 
directories and manipulating files without the file explorer, while 
capable of doing many more advanced things.  

To have a taste of what they are capable of, here are the most 
extensively used and probably the most useful Git and Bash commands 
based on my experience throughout the summer:  
* Clone from an existing repo by SSH
  ```bash
  $ git clone git@gitlab.com:user_name/my_project.git 
  ```
* Pull files from the remote repo so that it's up-to-date
  ```bash
  $ git pull 
  ```
* Create a new branch off current branch
  ```bash
  $ git branch myBranch 
  ```
* Migrate to myBranch
  ```bash
  $ git checkout myBranch 
  ```
* Check for files untracked, modified, waiting to be committed
  ```bash
  $ git status 
  ```
* Add myFile.txt to the staging area
  ```bash
  $ git add myFile.txt 
  ```
* Commit the staged change to local branch
  ```bash
  $ git commit -m "this is my commit message"
  ```
* Push the committed change to remote branch
  ```bash
  $ git push 
  ```
* Create new directory myDir
  ```bash
  $ mkdir myDir
  ```
* Switch current directory to myDir
  ```bash
  $ cd myDirectory 
  ```
* Remove all files and directories within trashDir
  ```bash
  $ rm -rf trashDir
  ```
* Rename myFile.txt as newFile.txt
  ```bash
  $ mv myFile.txt newFile.txt
  ```
* Copy all files and directories within oldDir to newDir
  ```bash
  $ cp -r oldDir/. newDir/.
  ```
* List all files and directories, including hidden ones, with detail
  ```bash
  $ ls -la
  ```

Here's a text-based game called 
[Terminus](http://web.mit.edu/mprat/Public/web/Terminus/Web/main.html) 
by MIT designed for those looking to learn and practice using the 
command line interface. We were introduced to this game by Jason during 
the first week of the internship and it was very helpful for practicing 
the commands in a fun setting. There's an amazing website called 
[Linux Journey](https://linuxjourney.com/) that comes with a 
[section on command line](https://linuxjourney.com/lesson/the-shell#), 
which is the more thorough tutorial we would turn to for reference.

Other than being able to use Git and Bash by the command-line 
interface, filing issue reports and submitting merge requests properly 
on Git-repository services like [GitLab](https://about.gitlab.com/) is 
something I've learned and exercised extensively during the summer 
internship which is also of great importance. It is essential for 
documenting and communicating with other team members so that necessary 
work gets done and no time is wasted on working on something that has 
already been done. 

It's safe to say that this Git and Bash workflow is one of the most 
valuable things I've learned through my internship. This is something 
I've not known or exercised before and I wish I had been introduced to 
it sooner. This is exactly why I've recommended the workflow to a few 
of my friends, who I'm sure will benefit from it the same way I did.

On a final note, I've only been using GitLab up to this point, but I'm 
confident that I can pick up using GitHub or any other Git-repository 
service given my experience with GitLab. Besides, all my development 
work is done on the Windows Subsystem for Linux environment installed 
on Visual Studio Code. Here's a link to 
[Developing in WSL](https://code.visualstudio.com/docs/remote/wsl) that 
interested viewers might find helpful to get started to code in WSL on 
Visual Studio Code. 

## Test-driven Development with `pytest`

A test-driven development workflow is one where you would write out 
the test cases first, and then build up program or feature aiming to 
pass the tests. By defining the expected behavior of the program 
beforehand, it's easier to keep track of the goals and create code 
that is less prone to bugs or unexpected behaviors. 

For the summer, I've taken up the specific approach of starting with 
writing out the function calls and defining tests that check if the 
function does return something and that the returned object is of the 
desired data type or if the result is correct when the function is 
doing something very simple. After the function has been mostly coded 
up, I'd run the function call test I've built up earlier to make sure 
they pass. I would then add a few more tests depending on the actual 
function, to thoroughly check if the function is performing as desired.  

There are a few ways to write tests in Python. One way is to use 
the [`unittest`](https://docs.python.org/3.7/library/unittest.html) 
package as we've been introduced in the _Computing for Finance in 
Python_ course taught by 
[Sebastien](https://finmath.uchicago.edu/about/faculty-and-lecturers/sebastien-donadio/). 
During the internship here at _Musicfox_, we were introduced to use 
the [`pytest`](https://docs.pytest.org/en/latest/) library to write 
test cases. 

The reason for favoring `pytest` over `unittest` is that writing tests 
is easier because you can just write test functions and don't have to 
create an entire test class as you would with `unittest`. In addition, 
the `pytest.fixture` object is a convenient and powerful tool for 
preparing resources used in the tests. You define a function that 
prepares what you need, let it be data input or desired results, and 
wrap it into the `@pytest.fixture` decorator. Then the function you 
defined can be simply passed as arguments into the the test functions 
for use. And of course, it provides very clear stack trace and error 
information for debugging. 

To use the testing feature, the name of the test functions must start 
with "test_". Otherwise `pytest` cannot figure out which functions to 
test. However, it is not required to have dedicated Python files to 
store the test functions, but it is recommended to stay organized and 
have the name of the Python file start with "test_" as well. Here's a 
very simple example where I have both the function and the test 
function stored in the same file [add_and_test.py](add_and_test.py).  
```python 
import pytest

@pytest.fixture
def cases(): 
    # simply return the dict with test cases and correct results
    return { 
        (1, 2): 3,
        (5, 2): 7,
        (8, 9): 17,
        (13, 93): 106,
        (7832, 8012): 15844,
    }

def test_add(cases): 
    for case, result in cases.items():
        add_result = add(*case)
        assert result == add_result

def add(num1, num2):
    return num1 + num2
```

To run the test of the function `add` using the file as specified 
above, simply type `pytest -vv add_and_test.py` in the command-line 
console and it will spit out the test result in a few moments if 
everything is correctly set up. The file name `add_and_test.py` here 
can be omitted if you wish to check for eligible tests in all files 
within the current directory. If, on the other hand, you have multiple 
test functions and you'd like to test only a few of them, add a `-k` 
option followed by a specific keyword for selection of tests.  

## Interactive Web App with `plotly/dash`

Before entering the internship, my only experience with front-end 
development was from an undergraduate course on web-page design, which 
was almost entirely taught without coding using Adobe Dreamweaver with 
occasional reference of HTML in the process.  

Here during the internship at _Musicfox_, I was introduced to the 
[`plotly/dash`](https://plot.ly/dash/) library, with which one can 
"build beautiful web-based analytic apps" and "no JavaScript required". 
This makes creating data visualization apps and features very easy for 
data scientists or financial engineers or generally any pure Python 
users with little experience on front-end application development or 
JavaScript.  

During my internship, the relating work I've done mostly involve the 
development of features that would be plugged into a greater framework 
of our _Musicfox_ web application. The first one I've built is a 3D 
graph that displays artist ranks in charts with the mean and volatility 
of changes in number of streams for a given set of artists. Following 
this one, I then worked on putting together the box chart of the number 
of streams for different types of tracks, categorized based on the 
record label and the number of composers and collaborators. Other than 
the two, I was also tasked with plotting an approximated Mean-Variance 
frontier for the universe of artists. The features I've created are in 
no shape or form mind-blowing, but the fact that I've got to know it's 
possible to visualize data and create interactive apps and features in 
pure Python, let alone actually gained first-hand experience in the 
process is massively valuable to me.

When learning the `plotly/dash` feature development, what I did was to 
first go through some of the 
[User Guides and Tutorials](https://dash.plot.ly/) provided. I also 
checked out the 
[Dash App Gallery](https://dash-gallery.plotly.host/Portal/), where a 
bunch of interactive data visualization sample apps are being 
displayed, to get a sense of what's possible with the library. Some of 
the examples have corresponding source codes that can be found in 
[their GitHub Repo](https://github.com/plotly/dash-sample-apps). The 
source codes of the sample apps found in the gallery can be quite 
complicated and a bit intimidating for beginners, but it's always a 
good idea to clone a few of the examples and play around or even try 
to break them to really learn how it works. I also link here 
[an article on interactive data visualization with plotly](https://towardsdatascience.com/its-2019-make-your-data-visualizations-interactive-with-plotly-b361e7d45dc6), 
which targets specifically data scientists using Python.

## _Musicfox_ Data Acquisition Library `pycm`

Since the start of the internship, we were introduced to the 
_MusicFox_ data acquisition library `pycm`. This was designed to be a 
wrapper around the 
[Chartmetric API](https://chartmetric.io/html/api.html) that provides 
access to artist- and consumption-related data from various streaming 
services. Throughout the summer, I've worked extensively both on and 
using the library, made many commits and pushes, and filed a bunch of 
issues and merge requests regarding it.

As an introductory first task, we started by coding up additional test 
cases, covering the standard, rare and extreme usages, for a selected 
few endpoints in the `pycm` library. This was meant to be more like 
an exercise that gave us a taste of the general structure and component 
of the library, and also writing Python tests using `pytest`.  

Later, when my fellow partner decided to focus on data scientist while 
I chose to go down the data engineering and software developing track, 
I got my first "for real" project as a data engineer to collect all 
obtainable charts (e.g. Spotify or AppleMusic) data from the 
Chartmetric API using our `pycm` library and store as coherent 
`DataFrame`. Due to the unfortunate fact that the API doesn't come with 
the best documentation in the world, it's often times quite confusing 
the structure of the data returned and the data type for each of the 
fields included. To further complicate the problem, the data fields 
included is different for different charts. For example, the Shazam 
charts contain a field `Number_of_Shazams`, which is nowhere to be 
found in the Spotify charts data. There are also dates where some of 
the expected data fields are completely missing in the raw return, 
making it crucial to have thorough exception handling all around. 
What's even worse is that the Chartmetric API limits the number of 
queries one can make in one minute, slowing down the testing of the 
functions and acquisition of the data. It further emphasized the 
importance of testing first with a limited sample and having proper 
exception handling, otherwise a lot of time could be wasted. All of 
these complications made it quite hard to build up a unified function 
that automatically detects the type of the raw charts and parse to 
form a `DataFrame` with expected columns. As such, this projected 
ended up spanning a little over two weeks, but I ultimately constructed 
the parser functions and wrappers for acquiring and updating the charts 
data, and used those to pull the charts of around 17 countries from 5 
different streaming services, dating back to December of 2017.

Following the completion, I took a turn from being the user of `pycm` 
to being the developer of the library, which was assigned to be my 
responsibility. The library was in working order, but still needed a 
lot of work - a number of Chartmetric endpoints were not covered; some 
of those covered were throwing errors and could not return meaningful 
data; tests and documentations were missing for some endpoints and were 
generally not thorough and clear enough.  

So my first step was to make sure the library has full coverage of the 
[Chartmetric API endpoints](https://api.chartmetric.com/apidoc/). I 
followed the workflow of beginning with function call and basic usage 
tests, and then moving on to the actual coding of the functions that 
cover the missing endpoints, and then on to a little bit more advanced 
tests checking if the endpoint gives back meaningful, non-empty 
returns. At this point, I was very new (still am) to the development 
game, so some of the endpoints I covered didn't come with documentation 
that needed rework, and the line lengths were generally not conforming 
to the 80-character rule. In hindsight, I also neglected to check the 
existing endpoints and corresponding tests for updates or fixes, which 
turned out to be something I would have to come back to and work on 
continuously later. 

Other than having the `pycm` functions covering all Chartmetric 
endpoints, I was in charge of using them to pull all available data to 
supply my data scientist partner at the same time. The scope of this 
task is obviously larger than that of the first one, where previously I 
only needed to acquire charts data from 5 streaming platforms, while 
all the previous complications remain. Despite these challenges, I 
managed to build up the wrapper functions that call the `pycm` 
functions and iteratively pull all available Chartmetric data. On a 
side note, it was in this process that I was made aware of several 
existing bugs that we didn't know of, mostly outdated functions that 
are no longer compatible with the Chartmetric endpoints. I was able to 
make use of GitLab to record the issues, work on the problem from a 
separate branch and submit merge requests to have the fix approved.  

Before ending the internship, I was tasked to go through the entire 
`pycm` library and refine the documentation, with the intention of 
having the library ready for open-source. Because the library was 
initially built in early 2019, the library suffered from missing, 
outdated and temporary documentation with working notes and to-dos. I 
managed to refactor, update, add and remove documentation and 
annotation where necessary, making sure the library provides clear and 
professional documentation that help users get started with ease.

## Technical Article Composition

For the duration of the internship, I've written a few technical 
articles, generally on programming in Python and development using Git 
and GitLab. These articles mostly came into being when I've run into 
and managed to solve some kind of tricky problems, or when I've learned 
about something new in development. To me, writing them down has been a 
great way to prperly formalize, document and review what I've learned. 
It also works as a guide and reminder to myself and others, so that the 
next time we run into the same problem and working on similar stuff we 
have something to refer to. Since _Musicfox_ has not yet made public of 
the articles I wrote during the internship, I can't link and cite them 
here in the summary for now. They will be up here as soon as I got the 
green light.

To begin with, during the first week of the internship, my partner and 
I wrote about the Git and Bash workflow that we employ at _Musicfox_. 
The technical article is actually quite similar in content to the first 
two parts of what I have here in the internship summary. The first week 
of internship was very exhilarating as I was introduced to this 
efficient workflow, and actually writing them down was a great 
opportunity for me to review for continuous learning. As I continued on 
with my internship, I've also made updates to reflect new tricks that I 
picked up along the way. 

After my first ever technical article, as I was going on the 
development track, Jason asked me to summarize with examples the parts 
about [List Comprehensions](https://docs.python.org/3/tutorial/datastructures.html#list-comprehensions) 
from the book [Effective Python](https://effectivepython.com/) (item 7 
to 9 of Chapter 1). I wrote about how one should use list 
comprehensions to enhance code readability and improve efficiency, with 
suggestions that match the title of the parts I'm refering to. The 
titles are pretty self explanatory and I quote them here as a friendly 
reminder to myself and to anyone actively programming in Python.  
* __Item 7:__ _Use list comprehensions instead of `map` and `filter`_  
* __Item 8:__ _Avoid more than two expressions in list comprehensions_  
* __Item 9:__ _Consider generator expressions for large comprehensions_  

Later, after I had developed a few data visualization features with 
`plotly/dash`, it made sense to take what I've learned and introduce 
the library, writing about how to develop web apps using the library, 
with a very simple examplar template. It was obviously not something 
super thorough and in-depth, but more like a getting-started guide for 
people like me that come from a Financial Mathematics background 
without a ton of experience in web development. Interested readers can 
refer to the third part of this summary, where I've included a few 
links that got me started and might be of help.

While performing the data engineering role, as I was using `pandas` and 
`json` all the time to work with and store the data, it's inevitable to 
have mistakenly coded up some bugs in the program. Therefore, I have 
also written a few articles to document the problems I've encountered 
and the corresponding fix, along with a minimum reproducible example 
for clearer illustration. Here, it makes sense to present two of such 
problems that I wrote about and explain the cause of the problems with 
brief examples.
 
The first one was encountered when I was using `pandas.concat` on a 
list of DataFrames with `axis=0` and got a 
`ValueError: Plan shapes are not aligned`. This happened because some 
of the DataFrames I was attempting to concat had columns 
`['Value', 'Timestp', 'Value', 'Timestp']`, and some had columns 
`['Value', 'Timestp']`. I was having DFs that looked like this because 
I had previously concated two data fields along `axis=1` and some of 
the data I got was missing one of the two data fields, resulting in DFs 
having not-aligned shapes. Here's a reproducible example that 
illustrates the problem.  
```python
import pandas as pd
import numpy as np

df1 = pd.DataFrame(
    np.random.randn(10, 4), 
    columns=['Value', 'Timestp', 'Value', 'Timestp']
)

df2 = pd.DataFrame(
    np.random.randn(10, 2), 
    columns=['Value', 'Timestp']
)
try:
    df = pd.concat([df1, df2], axis=0, sort=False)
except Exception as e:
    print(f"Concat error -> {e}")
```
To fix this problem, one way is to store the data separately to avoid 
having DataFrames like `df1`. Another way is to initialize with 
DataFrame filled with NaN, and effectively eliminate DataFrames like 
`df2`. A third way is to fix the initial part of concating along 
`axis=1`, setting the `Timestp` as the index, and rename each `Value` 
with the actual name of the data field. In this way, we won't be 
concating DataFrames that some have duplicated columns and some do not.

The second one of such problems arose when I was storing and reading 
DataFrames as json files using the built-in `pandas.DataFrame.to_json` 
function. Storing the DataFrames, which had MultiIndex, was completely 
trouble-free. However, when attempting to read the stored json file for 
the original DataFrame, I got a 
`ValueError: No ':' found when decoding object value.` The cause of the 
problem was exactly that the DataFrame had MultiIndex, and the `to_json` 
function is not properly built to handle this type of structure, as 
noted in this 
[issue on `pandas`](https://github.com/pandas-dev/pandas/issues/4889). 
Here's a reproducible example that demonstrates the problem.  
```python
import pandas as pd
import numpy as np

arrays = [[1, 1, 2, 2], ['red', 'blue', 'red', 'blue']]
mindex = pd.MultiIndex.from_arrays(arrays, names=('number', 'color'))

df = pd.DataFrame(
    np.random.randn(4, 2),
    columns=['data1', 'data2'],
    index = mindex
)

print(df)

try:
    dumped = df.to_json()
except Exception as e:
    print(f"Error in to_json -> {e}")
try:
    loaded = pd.read_json(dumped)
except Exception as e:
    print(f"Error in read_json -> {e}")
```
The way around is quite straightforward. Since the `to_json` function 
cannot properly handle MultiIndex, removing it with `reset_index` 
before sending it to json will do the trick perfectly. Simply use 
`set_index` to get back the MultiIndex structure after reading back in 
the json as DataFrame. 

## Conclusion

I'd like to reiterate just how much of a rewarding experience for me to 
have interned at _Musicfox_. Big shout out to 
[Jason](https://www.linkedin.com/in/thinkjrs/) and 
[Phil](https://www.linkedin.com/in/philipmuellerschoen/) for having me 
there over the summer and providing this awesome opportunity for me to 
program, research and learn in the progress. I also thank my classmate 
[Sherry](https://www.linkedin.com/in/shihui-sherry-yu-50a311107/) for 
working with me and helping me when needed. You can reach out to them 
by the LinkedIn links I provided here. Also, feel free to connect to 
[me](https://www.linkedin.com/in/yiyang-richard-shu-380a53170/) on 
LinkedIn or find my resume [here](Yiyang_Shu_Resume.pdf). 
Suggestions to this summary or other things I have up here are always 
welcome and appreciated!
