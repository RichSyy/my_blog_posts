"""
This is the accompanying Python file with example 
from `internship_summary.md`.
"""
import pytest


@pytest.fixture
def cases():
    return {
        (1, 2): 3,
        (5, 2): 7,
        (8, 9): 17,
        (13, 93): 106,
        (7832, 8012): 15844,
    }


def test_add(cases):
    for case, result in cases.items():
        add_result = add(*case)
        assert result == add_result


def add(num1, num2):
    return num1 + num2
    